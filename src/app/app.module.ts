import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonasComponent } from './components/personas/personas.component';
import { PersonasDetalleComponent } from './components/personas-detalle/personas-detalle.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { AboutComponent } from './components/about/about.component';
import { PersonasTarjetaComponent } from './components/personas-tarjeta/personas-tarjeta.component';
import { PersonasService } from './services/personas.service';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
//ng add @angular/material

@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    PersonasDetalleComponent,
    HomeComponent,
    NavbarComponent,
    AboutComponent,
    PersonasTarjetaComponent,
    BuscadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [PersonasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
