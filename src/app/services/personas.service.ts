import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private personas:Persona[] =
[
  {
    rut:"44444-6",
    nombres: "Covid",
    apPat: "Aurelio",
    apMat: "Donoso",
    img: "assets/img/1.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"99776739964-6",
    nombres: "Luis Enrique",
    apPat: "Figo",
    apMat: "Sánchez",
    img: "assets/img/2.png",
    fnac: "1941-11-01",
    vigente: false
  },
  {
    rut:"9939964-6",
    nombres: "José",
    apPat: "Montenegro",
    apMat: "Ayarza",
    img: "assets/img/3.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"99399864-6",
    nombres: "Britny",
    apPat: "Spritz",
    apMat: "Zerg",
    img: "assets/img/1m.png",
    fnac: "1978-05-11",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Sandra",
    apPat: "O'Ryan",
    apMat: "Mal",
    img: "assets/img/2m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Julia",
    apPat: "The Great",
    apMat: "Iglesia",
    img: "assets/img/3m.png",
    fnac: "1941-11-01",
    vigente: false
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  },
  {
    rut:"9939964-6",
    nombres: "Rosa",
    apPat: "Espinoza",
    apMat: "Noma",
    img: "assets/img/4m.png",
    fnac: "1941-11-01",
    vigente: true
  }
];
  constructor() {
    console.log("El servicio está listo para trabajar!!");
   }

   getPersonas(){
    return this.personas;
  }

  getPersona(idx:string){
    return this.personas[idx];
  }

  buscarPersonas(termino: string):Persona[]{
    let personasArr:Persona[] = [];
    termino = termino.toLowerCase();

    for(let i=0; i<this.personas.length; i++){
        
      let persona = this.personas[i];
      let nombres = persona.nombres.toLowerCase();

      //indexOf : si no pilla coincidencia devuelve -1
      if(nombres.indexOf(termino) >= 0) {
        persona.idx = i;
        personasArr.push( persona );
      }
    }
    return personasArr;
  }
}

export interface Persona {
  rut: string;
  nombres: string;
  apPat: string;
  apMat: string;
  img:  string;
  fnac: string;
  vigente: boolean;
  idx?: number;
}
