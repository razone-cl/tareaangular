import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonasService, Persona } from '../../services/personas.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss']
})
export class BuscadorComponent implements OnInit {

  personas : any[] = [];
  termino: string;

  constructor(private activatedRoute: ActivatedRoute, private _personasService: PersonasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.termino = params['termino'];
      this.personas = this._personasService.buscarPersonas(params['termino']);
      console.log(this.personas); 
    })
  }

}
