import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personas-tarjeta',
  templateUrl: './personas-tarjeta.component.html',
  styleUrls: ['./personas-tarjeta.component.scss']
})
export class PersonasTarjetaComponent implements OnInit {

  @Input() persona: any = {};
  @Input() index: number ;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  verPersona(){
    this.router.navigate(['/personas-detalle', this.index]);
  }

}
