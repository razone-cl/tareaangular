import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasTarjetaComponent } from './personas-tarjeta.component';

describe('PersonasTarjetaComponent', () => {
  let component: PersonasTarjetaComponent;
  let fixture: ComponentFixture<PersonasTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
