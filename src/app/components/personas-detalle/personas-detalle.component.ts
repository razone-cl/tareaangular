import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; //capturar un valor de un param dinámico
import { PersonasService } from "../../services/personas.service";

@Component({
  selector: 'app-personas-detalle',
  templateUrl: './personas-detalle.component.html',
  styleUrls: ['./personas-detalle.component.scss']
})
export class PersonasDetalleComponent {

  persona: any = {};

  constructor(private activatedRoute: ActivatedRoute, private _personasService: PersonasService) {
      this.activatedRoute.params.subscribe(params => {
      this.persona = this._personasService.getPersona(params['id']);
      console.log(this.persona);
   });

  }
  

}
