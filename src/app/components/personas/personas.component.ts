import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; //capturar un valor de un param dinámico
import { PersonasService, Persona } from "../../services/personas.service";

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.scss']
})
export class PersonasComponent implements OnInit {

  personas: Persona[] = [];

  constructor(private router: Router, private _personasService: PersonasService) { }

  ngOnInit() {
    this.personas = this._personasService.getPersonas();
  }

  verPersona(idx: number){
    this.router.navigate(['personas-detalle', idx]);
  }

}
