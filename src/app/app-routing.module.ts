import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PersonasComponent } from './components/personas/personas.component';
import { PersonasDetalleComponent } from './components/personas-detalle/personas-detalle.component';
import { AboutComponent } from './components/about/about.component';
import { BuscadorComponent } from './components/buscador/buscador.component';



const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'personas', component: PersonasComponent},
    {path: 'personas-detalle/:id', component: PersonasDetalleComponent},
    {path: 'about', component: AboutComponent},
    {path: 'buscar/:termino', component: BuscadorComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
